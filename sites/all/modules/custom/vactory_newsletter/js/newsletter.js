(function ($) {
  Drupal.behaviors.newsletter = {
    attach: function (context, settings) {


      $("input[name='email']", context).click(function () {
          if ($(this).val() == Drupal.t('user@example.com')) {
            $(this).val('');
          }
      });
      $("input[name='email']", context).blur(function () {
        if ($(this).val() == '') {
          $(this).val(Drupal.t('user@example.com'));
        }
      });
      if($('.asFixedButton').length) {
        $('#newsletter .asFixedButton .button').on('click', function() {
          if($(this).hasClass('isClosed')) {
            $(this).removeClass('isClosed');
            $('.asFixedButton').removeClass('isClosed');
          }
          else {
            $(this).addClass('isClosed');
            $('.asFixedButton').addClass('isClosed');
          }

        });
      }

      if($('.asWidget').length) {
        $('#newsletter .asWidget h4').on('click', function() {
          if($(this).hasClass('isClosed')) {
            $(this).removeClass('isClosed').next().removeClass('isClosed');
          }
          else {
            $(this).addClass('isClosed').next().addClass('isClosed');
          }

        });
      }

    },

    subscribeForm: function(data) {
      $.each(Drupal.settings.exposed, function(e,i) {
        if (!$('#edit-field-newsletter-list-' + Drupal.settings.lang + '-' + i).attr('checked')) {
          $('.form-item-exposed-' + i).hide();
        }

        $('#edit-field-newsletter-list-' + Drupal.settings.lang + '-' + i).click(function () {
          $('.form-item-exposed-' + i).toggle();
        });
      });
    }

  };


})(jQuery);


jQuery(document).ready(function() {
  var counter=0;
  jQuery('input.form-checkbox').on('change', function () {

    if (jQuery('input[type=checkbox]:checked').length > 4) {
      jQuery(this).prop('checked', false);
      if(counter<=0){
        //jQuery('.field-name-field-cat-gories').next('.alerte').remove();
        jQuery('.field-name-field-cat-gories').after('<div class="alert alert-danger alert-dismissable">'+Drupal.t('Only 3 Items          are alowed')+'</div>');
        counter ++;
      }


    }
  });
});