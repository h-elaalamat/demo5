<div id="newsletter">
    <div class="<?php if(isset($type)): print $type ;endif; ?> wrapper isClosed">
        <div class="container">
            <div class="button isClosed"><span>Newsletter</span></div>
            <h4 class="block__title">
                <?php print $block_title ?>
            </h4>
            <div class="block__content">
                <div class="prefix">
                    <?php print $block_subtitle ?>

                </div>
                <?php print render($content)  ?>
            </div>
        </div>
    </div>
</div>



