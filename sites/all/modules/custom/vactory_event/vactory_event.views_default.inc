<?php
/**
 * @file
 * vactory_event.views_default.inc
 */



/**
 * Implements hook_views_default_views().
 */
function vactory_event_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'vactory_events_push';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Agenda';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '2';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event' => 'event',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'field_vactory_event_category_tid' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => 'Filtrer par thème',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            1 => 'vocabulary',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'field_vactory_date_value' => array(
      'bef_format' => 'bef_datepicker',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'secondary_label' => NULL,
    'collapsible_label' => NULL,
    'combine_rewrite' => NULL,
    'reset_label' => NULL,
    'bef_filter_description' => NULL,
    'any_label' => NULL,
    'filter_rewrite_values' => NULL,
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 0;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'row';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Image Cover */
  $handler->display->display_options['fields']['field_vactory_image_cover']['id'] = 'field_vactory_image_cover';
  $handler->display->display_options['fields']['field_vactory_image_cover']['table'] = 'field_data_field_vactory_image_cover';
  $handler->display->display_options['fields']['field_vactory_image_cover']['field'] = 'field_vactory_image_cover';
  $handler->display->display_options['fields']['field_vactory_image_cover']['label'] = '';
  $handler->display->display_options['fields']['field_vactory_image_cover']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_vactory_image_cover']['element_class'] = 'pull-left';
  $handler->display->display_options['fields']['field_vactory_image_cover']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_image_cover']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_image_cover']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_vactory_image_cover']['settings'] = array(
    'image_style' => 'vactory_event_cover_style_large',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_vactory_image_cover']['field_api_classes'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_vactory_event_category']['id'] = 'field_vactory_event_category';
  $handler->display->display_options['fields']['field_vactory_event_category']['table'] = 'field_data_field_vactory_event_category';
  $handler->display->display_options['fields']['field_vactory_event_category']['field'] = 'field_vactory_event_category';
  $handler->display->display_options['fields']['field_vactory_event_category']['label'] = '';
  $handler->display->display_options['fields']['field_vactory_event_category']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_event_category']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_event_category']['field_api_classes'] = TRUE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_vactory_date']['id'] = 'field_vactory_date';
  $handler->display->display_options['fields']['field_vactory_date']['table'] = 'field_data_field_vactory_date';
  $handler->display->display_options['fields']['field_vactory_date']['field'] = 'field_vactory_date';
  $handler->display->display_options['fields']['field_vactory_date']['label'] = '';
  $handler->display->display_options['fields']['field_vactory_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  $handler->display->display_options['fields']['field_vactory_date']['field_api_classes'] = TRUE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['field_api_classes'] = TRUE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event' => 'event',
  );
  /* Filter criterion: Content: Category (field_vactory_event_category) */
  $handler->display->display_options['filters']['field_vactory_event_category_tid']['id'] = 'field_vactory_event_category_tid';
  $handler->display->display_options['filters']['field_vactory_event_category_tid']['table'] = 'field_data_field_vactory_event_category';
  $handler->display->display_options['filters']['field_vactory_event_category_tid']['field'] = 'field_vactory_event_category_tid';
  $handler->display->display_options['filters']['field_vactory_event_category_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_vactory_event_category_tid']['expose']['operator_id'] = 'field_vactory_event_category_tid_op';
  $handler->display->display_options['filters']['field_vactory_event_category_tid']['expose']['operator'] = 'field_vactory_event_category_tid_op';
  $handler->display->display_options['filters']['field_vactory_event_category_tid']['expose']['identifier'] = 'field_vactory_event_category_tid';
  $handler->display->display_options['filters']['field_vactory_event_category_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_vactory_event_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_vactory_event_category_tid']['vocabulary'] = 'event_category';
  /* Filter criterion: Content: Date -  start date (field_vactory_date) */
  $handler->display->display_options['filters']['field_vactory_date_value']['id'] = 'field_vactory_date_value';
  $handler->display->display_options['filters']['field_vactory_date_value']['table'] = 'field_data_field_vactory_date';
  $handler->display->display_options['filters']['field_vactory_date_value']['field'] = 'field_vactory_date_value';
  $handler->display->display_options['filters']['field_vactory_date_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_vactory_date_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_vactory_date_value']['expose']['operator_id'] = 'field_vactory_date_value_op';
  $handler->display->display_options['filters']['field_vactory_date_value']['expose']['label'] = 'Filtrer par date';
  $handler->display->display_options['filters']['field_vactory_date_value']['expose']['operator'] = 'field_vactory_date_value_op';
  $handler->display->display_options['filters']['field_vactory_date_value']['expose']['identifier'] = 'field_vactory_date_value';
  $handler->display->display_options['filters']['field_vactory_date_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_vactory_date_value']['group_info']['label'] = 'Date -  start date (field_vactory_date)';
  $handler->display->display_options['filters']['field_vactory_date_value']['group_info']['identifier'] = 'field_vactory_date_value';
  $handler->display->display_options['filters']['field_vactory_date_value']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'start date',
      'operator' => '>=',
      'value' => array(
        'value_group' => array(
          'value_choose_input_type' => 'date',
          'value' => NULL,
          'default_date' => '',
        ),
        'min_group' => array(
          'min_choose_input_type' => 'date',
          'min' => NULL,
          'default_date' => '',
        ),
        'max_group' => array(
          'max_choose_input_type' => 'date',
          'max' => NULL,
          'default_to_date' => '',
        ),
      ),
    ),
    2 => array(
      'title' => 'end date',
      'operator' => '>=',
      'value' => array(
        'value_group' => array(
          'value_choose_input_type' => 'date',
          'value' => NULL,
          'default_date' => '',
        ),
        'min_group' => array(
          'min_choose_input_type' => 'date',
          'min' => NULL,
          'default_date' => '',
        ),
        'max_group' => array(
          'max_choose_input_type' => 'date',
          'max' => NULL,
          'default_to_date' => '',
        ),
      ),
    ),
  );
  $handler->display->display_options['filters']['field_vactory_date_value']['form_type'] = 'date_text';
  $handler->display->display_options['path'] = 'events';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'row';
  $handler->display->display_options['defaults']['use_more'] = FALSE;
  $handler->display->display_options['use_more'] = TRUE;
  $handler->display->display_options['defaults']['use_more_always'] = FALSE;
  $handler->display->display_options['defaults']['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['defaults']['use_more_text'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'See more Agenda';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'col-md-8 col-xs-12';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['footer'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Image Cover */
  $handler->display->display_options['fields']['field_vactory_image_cover']['id'] = 'field_vactory_image_cover';
  $handler->display->display_options['fields']['field_vactory_image_cover']['table'] = 'field_data_field_vactory_image_cover';
  $handler->display->display_options['fields']['field_vactory_image_cover']['field'] = 'field_vactory_image_cover';
  $handler->display->display_options['fields']['field_vactory_image_cover']['label'] = '';
  $handler->display->display_options['fields']['field_vactory_image_cover']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_vactory_image_cover']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_vactory_image_cover']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_image_cover']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_vactory_image_cover']['settings'] = array(
    'image_style' => 'vactory_event_cover_style_large',
    'image_link' => 'content',
    'field_formatter_class' => '',
  );
  $handler->display->display_options['fields']['field_vactory_image_cover']['field_api_classes'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_vactory_date']['id'] = 'field_vactory_date';
  $handler->display->display_options['fields']['field_vactory_date']['table'] = 'field_data_field_vactory_date';
  $handler->display->display_options['fields']['field_vactory_date']['field'] = 'field_vactory_date';
  $handler->display->display_options['fields']['field_vactory_date']['label'] = '';
  $handler->display->display_options['fields']['field_vactory_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_vactory_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 1,
  );
  $handler->display->display_options['fields']['field_vactory_date']['field_api_classes'] = TRUE;
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_vactory_event_category']['id'] = 'field_vactory_event_category';
  $handler->display->display_options['fields']['field_vactory_event_category']['table'] = 'field_data_field_vactory_event_category';
  $handler->display->display_options['fields']['field_vactory_event_category']['field'] = 'field_vactory_event_category';
  $handler->display->display_options['fields']['field_vactory_event_category']['label'] = '';
  $handler->display->display_options['fields']['field_vactory_event_category']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_vactory_event_category']['element_type'] = '0';
  $handler->display->display_options['fields']['field_vactory_event_category']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_event_category']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_vactory_event_category']['element_wrapper_class'] = 'btn-primary';
  $handler->display->display_options['fields']['field_vactory_event_category']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_event_category']['field_api_classes'] = TRUE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_type'] = '0';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '400',
  );
  $handler->display->display_options['fields']['body']['field_api_classes'] = TRUE;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = 'En savoir plus';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="col-xs-5">
    [field_vactory_image_cover]
</div>
<div class="col-xs-7">
    <div>
        <div>
           [field_vactory_date] 
           <h3>[title]</h3>
           <span class="btn">[field_vactory_event_category]</span>
            [body]
            [view_node]
        </div>
    </div>
</div>
';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event' => 'event',
  );
  /* Filter criterion: Content: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'node';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
  );

  /* Display: Attachment */
  $handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'col-md-4';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '4';
  $handler->display->display_options['pager']['options']['offset'] = '1';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'row';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Image Cover */
  $handler->display->display_options['fields']['field_vactory_image_cover']['id'] = 'field_vactory_image_cover';
  $handler->display->display_options['fields']['field_vactory_image_cover']['table'] = 'field_data_field_vactory_image_cover';
  $handler->display->display_options['fields']['field_vactory_image_cover']['field'] = 'field_vactory_image_cover';
  $handler->display->display_options['fields']['field_vactory_image_cover']['label'] = '';
  $handler->display->display_options['fields']['field_vactory_image_cover']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_vactory_image_cover']['element_class'] = 'pull-left';
  $handler->display->display_options['fields']['field_vactory_image_cover']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_image_cover']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_image_cover']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_vactory_image_cover']['settings'] = array(
    'image_style' => 'vactory_event_cover_style_small',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_vactory_image_cover']['field_api_classes'] = TRUE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_vactory_date']['id'] = 'field_vactory_date';
  $handler->display->display_options['fields']['field_vactory_date']['table'] = 'field_data_field_vactory_date';
  $handler->display->display_options['fields']['field_vactory_date']['field'] = 'field_vactory_date';
  $handler->display->display_options['fields']['field_vactory_date']['label'] = '';
  $handler->display->display_options['fields']['field_vactory_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  $handler->display->display_options['fields']['field_vactory_date']['field_api_classes'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event' => 'event',
  );
  /* Filter criterion: Content: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'node';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
  );
  $handler->display->display_options['displays'] = array(
    'block' => 'block',
    'default' => 0,
    'page' => 0,
  );
  $handler->display->display_options['attachment_position'] = 'after';
  $translatables['vactory_events_push'] = array(
    t('Master'),
    t('Agenda'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Page'),
    t('Advanced options'),
    t('Filtrer par thème'),
    t('Select any filter and click on Apply to see results'),
    t('Filtrer par date'),
    t('Date -  start date (field_vactory_date)'),
    t('Block'),
    t('See more Agenda'),
    t('En savoir plus'),
    t('<div class="col-xs-5">
    [field_vactory_image_cover]
</div>
<div class="col-xs-7">
    <div>
        <div>
           [field_vactory_date] 
           <h3>[title]</h3>
           <span class="btn">[field_vactory_event_category]</span>
            [body]
            [view_node]
        </div>
    </div>
</div>
'),
    t('Attachment'),
  );
  $export['vactory_events_push'] = $view;

  return $export;
}
