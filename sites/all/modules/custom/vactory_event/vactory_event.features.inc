<?php
/**
 * @file
 * vactory_event.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function vactory_event_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function vactory_event_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function vactory_event_image_default_styles() {
  $styles = array();

  // Exported image style: vactory_event_cover_style_large.
  $styles['vactory_event_cover_style_large'] = array(
    'label' => 'Event Cover Style Large (300x350)',
    'effects' => array(
      1 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 300,
          'height' => 350,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_event_cover_style_small.
  $styles['vactory_event_cover_style_small'] = array(
    'label' => 'Event Cover Style Small (65x75)',
    'effects' => array(
      2 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 65,
          'height' => 75,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function vactory_event_node_info() {
  $items = array(
    'event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => t('This content type allow create events with a category and from/to date'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
